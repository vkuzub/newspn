package com.vkuzub.newspn.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.fragments.workers.TaskCallbacks;
import com.vkuzub.newspn.fragments.workers.WorkerFragmentArticle;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.utils.AsyncImageDownloader;
import com.vkuzub.newspn.utils.PNUtils;
import com.vkuzub.newspn.xml.XmlWorker;

import java.io.IOException;
import java.util.List;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class ArticleActivity extends ActionBarActivity implements TaskCallbacks {

    private ProgressDialog progressDialog;

    private WorkerFragmentArticle workerFragment;
    private boolean downloadMoreMedia;

    private TextView tvHeader, tvBody;
    private ImageView ivArticleHeader;
    private LinearLayout llMedia;

    private int displayWidth;
    private String articleUrl;
    private Article article;

    public String getArticleUrl() {
        return articleUrl;
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        articleUrl = getIntent().getStringExtra(PNUtils.INTENT_URL_EXTRA);
        setContentView(R.layout.fragment_article);
        initWidgets();
        readPreferences();

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        if (!PNUtils.WORK_MODE.isOnline()) {
            try {
                article = (Article) getIntent().getSerializableExtra(PNUtils.INTENT_CACHE_ARTICLE);
                articleUrl = article.getArticleUrl();
                Log.d("MyLogs", getClass().getCanonicalName() + " getFromIntentArticle");
                setArticleContent(article);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (savedInstanceState != null) {
                article = (Article) savedInstanceState.getSerializable(PNUtils.BUNDLE_ARTICLE_TAG);
                Log.d("MyLogs", getClass().getCanonicalName() + " getFromBundleSavedInstance");
                setArticleContent(article);
                return;
            }
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        workerFragment = (WorkerFragmentArticle) fragmentManager.findFragmentByTag(PNUtils.TAG_WORKER_FRAGMENT);

        if (workerFragment == null) {
            Bundle args = new Bundle();
            args.putString("url", articleUrl);
            workerFragment = new WorkerFragmentArticle();
            workerFragment.setArguments(args);
            fragmentManager.beginTransaction().add(workerFragment, PNUtils.TAG_WORKER_FRAGMENT).commit();
        }

    }

    private void initWidgets() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvBody = (TextView) findViewById(R.id.tvArticleBody);
        llMedia = (LinearLayout) findViewById(R.id.llMedia);
        ivArticleHeader = (ImageView) findViewById(R.id.ivArticleHeader);
        ivArticleHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.article, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuArticleShare:

                if (article != null) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.recomend_to_read));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.recomend_to_read) + " " + article.getTitle() + " " + articleUrl);
                    startActivity(shareIntent);
                } else {
                    Toast.makeText(this, getString(R.string.publication_not_in_cache), Toast.LENGTH_SHORT).show();
                }

                break;
//            case android.R.id.home:
//                this.finish();
//                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPreExecute() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.updating));
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object object) {
        Article article = (Article) object;
        try {
            setArticleContent(article);
            XmlWorker worker = new XmlWorker(getApplicationContext());
            worker.writeArticle(article);
        } catch (NullPointerException e) {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        progressDialog.dismiss();
    }

    @Override
    public void onProgressUpdate(int percent) {

    }

    @Override
    public void onCancelled() {

    }

    void readPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        downloadMoreMedia = sharedPreferences.getBoolean(getResources().getString(R.string.preferences_download_more_media_key), false);
    }

    public void setArticleContent(Article article) {
        this.article = article;
        setHeader(article.getTitle());
        setBody(article.getBody());
        setArticleHeader(article.getHeaderPictureUrl());
        if (downloadMoreMedia && PNUtils.WORK_MODE.isOnline()) {
            Toast.makeText(getApplicationContext(), getString(R.string.images_downloading), Toast.LENGTH_SHORT).show();
            setMedia(article.getMediaFiles());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("MyLogs", getClass().getCanonicalName() + "onSaveInstance");
        outState.putSerializable(PNUtils.BUNDLE_ARTICLE_TAG, article);
    }

    public void setHeader(String header) {
        tvHeader.setText(header);
    }

    public void setBody(String body) {
        tvBody.setText(body);
    }

    public void setArticleHeader(final String url) {
        if (url == null) {
            ivArticleHeader.setImageDrawable(getResources().getDrawable(R.drawable.emptynews));
            return;
        }
        Display display = getWindowManager().getDefaultDisplay();
        //noinspection deprecation
        displayWidth = display.getWidth();
        ivArticleHeader.setTag(url);
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(displayWidth);
        imageDownloader.execute(ivArticleHeader);
    }

    public void setMedia(List<String> media) {
        ImageView image;
        AsyncImageDownloader imageDownloader;
        for (String mediaUrl : media) {
            image = new ImageView(this);
            image.setTag(mediaUrl);
            imageDownloader = new AsyncImageDownloader(displayWidth);
            imageDownloader.execute(image);
            llMedia.addView(image);
        }
    }

}