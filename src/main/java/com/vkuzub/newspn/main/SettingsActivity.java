package com.vkuzub.newspn.main;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.*;
import android.widget.Toast;
import com.vkuzub.newspn.R;

/**
 * Created by Vyacheslav on 28.07.2014.
 */
public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private CheckBoxPreference checkBoxUpdateOnStart, checkBoxDownloadMedia;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPreferences();
    }

    @SuppressWarnings("deprecation")
    private void initPreferences() {
        PreferenceScreen preferenceScreen = getPreferenceManager().createPreferenceScreen(this);

        setPreferenceScreen(preferenceScreen);

        PreferenceCategory mainCategory, otherCategory;

        mainCategory = new PreferenceCategory(this);
        mainCategory.setTitle("Настройки приложения");

        checkBoxUpdateOnStart = new CheckBoxPreference(this);
        checkBoxUpdateOnStart.setKey(getResources().getString(R.string.preferences_update_onstart_key));
        checkBoxUpdateOnStart.setTitle(getResources().getString(R.string.preferences_onstartupdate));
        checkBoxUpdateOnStart.setSummary(getResources().getString(R.string.preferences_onstartupdate_summary));
        checkBoxUpdateOnStart.setChecked(true);


        checkBoxDownloadMedia = new CheckBoxPreference(this);
        checkBoxDownloadMedia.setKey(getResources().getString(R.string.preferences_download_more_media_key));
        checkBoxDownloadMedia.setTitle(getResources().getString(R.string.preferences_downloadmoremedia));
        checkBoxDownloadMedia.setSummary(getResources().getString(R.string.preferences_downloadmoremedia_summary));

        otherCategory = new PreferenceCategory(this);
        otherCategory.setTitle("Прочее");

        preferenceScreen.addPreference(mainCategory);
        preferenceScreen.addPreference(checkBoxUpdateOnStart);
        preferenceScreen.addPreference(checkBoxDownloadMedia);

        preferenceScreen.setDependency(getResources().getString(R.string.preferences_update_onstart_key));
        preferenceScreen.setDependency(getResources().getString(R.string.preferences_download_more_media_key));
    }

    private void savePreferences() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(getResources().getString(R.string.preferences_update_onstart_key), checkBoxUpdateOnStart.isChecked());
        editor.putBoolean(getResources().getString(R.string.preferences_download_more_media_key), checkBoxDownloadMedia.isChecked());
        editor.commit();
    }


    @Override
    protected void onDestroy() {
        savePreferences();
        super.onDestroy();
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(getString(R.string.preferences_clear_cache_key))) {
            Toast.makeText(this, "Очищается кэш", Toast.LENGTH_SHORT).show();
        }
    }
}