package com.vkuzub.newspn.parser;

import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.model.ArticleItem;
import com.vkuzub.newspn.model.BlogItem;
import com.vkuzub.newspn.model.NewsItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vyacheslav on 18.07.2014.
 * Класс - парсер
 */
public class JsoupHtmlParser {

//    public static void main(String[] args) throws IOException {

//        System.out.println(parseNewsList("https://news.pn/ru/archive/20140721/"));
//        System.out.println(parseArticle("https://news.pn/ru/public/109421"));
//        System.out.println(parseBlogsList("https://news.pn/ru/blogs/"));
//        System.out.println(parseArticlesList("https://news.pn/ru/articles/?page=2"));
//    }

    public static List<NewsItem> parseNewsList(String url) throws IOException {
        boolean isUrlNull = url == null ? true : url.length() == 0;
        if (isUrlNull) {
            return new ArrayList<NewsItem>();
        }

        ArrayList<NewsItem> news = new ArrayList<NewsItem>();
        Document doc;
        Element body;

        doc = Jsoup.connect(url).get();
        body = doc.body();


        Elements ul = body.getElementsByAttributeValueContaining("class", "publications");
        String name;
        String time;
        String itemUrl;
        NewsItem.NewsItemSeriousness seriousness = NewsItem.NewsItemSeriousness.Normal;

        //определяем, время название и ссылку новости
        for (Element li : ul.get(0).children()) {
            name = li.text().substring(5, li.text().length());
            time = li.text().substring(0, 5);
            itemUrl = NewsItem.URL_BASE + li.getAllElements().attr("href");

            //определяем важность новости
            String seriousnessClass = li.getElementsByTag("a").get(0).attributes().get("class").trim();
            if (seriousnessClass.equals("bold")) {
                seriousness = NewsItem.NewsItemSeriousness.Bold;
            } else if (seriousnessClass.equals("red bold")) {
                seriousness = NewsItem.NewsItemSeriousness.RedBold;
            }

            news.add(new NewsItem(name, time, itemUrl, seriousness));
        }
        return news;
    }


    //парсит новости, статьи и блоги. Некоторые картинки не загружает. Пока
    public static Article parseArticle(String url) throws IOException {
        boolean isUrlNull = url == null ? true : url.length() == 0;
        if (isUrlNull) {
            return new Article(null, null, null, null, null);
        }
        Document doc;
        StringBuilder text = new StringBuilder();

        doc = Jsoup.connect(url).get();


        Element body = doc.body();
        Elements classContainer = body.getElementsByAttributeValue("class", "container");

        //парсим текст
        for (Element element : classContainer.get(0).getElementsByTag("p")) {
            text.append(element.text()).append("\n\n");
        }

        //парсим картинки
        ArrayList<String> media = new ArrayList<String>();
        for (Element a : classContainer.get(0).getElementsByAttributeValue("rel", "pubphoto")) {
            media.add("http://" + a.attributes().get("href").substring(2));
        }

        String header;
        String articleHeaderUrl = null;

        //парсим название статьи
        header = classContainer.get(0).getElementsByTag("h1").text();

        if (media.size() > 0) {
            articleHeaderUrl = media.get(0);
            media.remove(articleHeaderUrl);
        }


//        <div class="container">
//        <div style="margin-top:10px" class="row content">
//        <article class="ninecol publication lzl hentry">
//        <header>
//        <h1 class="entry-title post-title"><a href="/ru/incidents/109226" title="Главный «ополченец» Николаева Янцен намерен люстрировать местных общественников" rel="bookmark">Главный «ополченец» Николаева Янцен намерен люстрировать местных общественников</a></h1>

        String urlnumbers = null;       //id статьи для записи в xml
        Pattern pattern = Pattern.compile("[0-9]+");

        Matcher matcher = pattern.matcher(url);

        while (matcher.find()) {
            urlnumbers = matcher.group();
        }

        return new Article(header, text.toString(), media, articleHeaderUrl, urlnumbers);
    }

    public static List<ArticleItem> parseArticlesList(String url) throws IOException {
        boolean isUrlNull = url == null ? true : url.length() == 0;
        if (isUrlNull) {
            return new ArrayList<ArticleItem>();
        }
        ArrayList<ArticleItem> articles = new ArrayList<ArticleItem>();

        Document doc;
        Element body;

        doc = Jsoup.connect(url).get();
        body = doc.body();


        String name;
        String time;
        String article_url;
        String picurl;
        String article_summary;

        Elements div = body.getElementsByAttributeValue("class", "row content");
        for (Element element : div.get(0).getElementsByTag("li")) {
            //берем все записи из списка содержащие атрибут class=hentry
            if (element.attributes().get("class").contains("hentry")) {
//                System.out.println(element);
                name = element.getElementsByTag("img").get(0).attributes().get("title");
                time = element.getElementsByAttributeValue("class", "published").text();
                article_url = element.getElementsByTag("a").get(0).attributes().get("href");
                picurl = element.getElementsByTag("img").get(0).attributes().get("src");
                article_summary = element.getElementsByAttributeValue("class", "entry-summary").text();
                articles.add(new ArticleItem(name, time, ArticleItem.URL_BASE + article_url, picurl.substring(2), article_summary));
            }
        }
        return articles;
    }

    public static List<BlogItem> parseBlogsList(String url) throws IOException {
        boolean isUrlNull = url == null ? true : url.length() == 0;
        if (isUrlNull) {
            return new ArrayList<BlogItem>();
        }
        ArrayList<BlogItem> blogs = new ArrayList<BlogItem>();
        Document doc;

        Element body;

        doc = Jsoup.connect(url).get();
        body = doc.body();

        String name;
        String time;
        String blog_url;
        String picurl;
        String article_summary;
        String author;

        //получили ul - список с названием
        Elements ul = body.getElementsByAttributeValueContaining("class", "feedBlogsAll");
        for (Element li : ul.get(0).children()) {
            //проверка что элемент - статья
            if (li.attributes().get("class").contains("hentry")) {
                Element a = li.getAllElements().first();

                //  элементы <a>...</a>
                for (Element element : a.children()) {
                    blog_url = BlogItem.URL_BASE + element.attributes().get("href");
                    name = element.getElementsByAttributeValue("class", "entry-title").text();
                    picurl = element.getElementsByTag("img").get(0).attributes().get("src");
                    time = element.getElementsByAttributeValue("class", "published").get(0).text();
                    author = element.getElementsByAttributeValue("class", "author").get(0).text();
                    article_summary = element.getElementsByAttributeValue("class", "entry-summary").get(0).text();
                    blogs.add(new BlogItem(name, time, blog_url, picurl.substring(2), article_summary.substring(0, article_summary.length() - 6), author));
                }
            }

        }
        return blogs;
    }

}
