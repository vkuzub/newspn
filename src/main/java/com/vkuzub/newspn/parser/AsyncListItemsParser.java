package com.vkuzub.newspn.parser;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.base.BaseListFragment;

import java.io.IOException;
import java.util.List;

/**
 * Created by Vyacheslav on 19.07.2014.
 *
 * @deprecated
 */
public class AsyncListItemsParser extends AsyncTask<String, Void, List<? extends BaseItem>> {

    private BaseListFragment baseListFragment;
    private ProgressDialog pd;
    private char type;

    public AsyncListItemsParser(char type) {
        this.type = type;
    }

    public void link(BaseListFragment baseListFragment) {
        this.baseListFragment = baseListFragment;
    }

    public void unlink() {
        baseListFragment = null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        pd = new ProgressDialog(activity);
//        pd.setMessage("Loading...");
//        pd.show();
    }

    @Override
    protected List<? extends BaseItem> doInBackground(String... strings) {
        Log.d("MyLogs", "parser" + type);
        Log.d("MyLogs", "url:" + strings[0]);
        List<? extends BaseItem> items = null;
        try {
            switch (type) {
                case 'a':
                    items = JsoupHtmlParser.parseArticlesList(strings[0]);
                    break;
                case 'b':
                    items = JsoupHtmlParser.parseBlogsList(strings[0]);
                    break;
                case 'n':
                    items = JsoupHtmlParser.parseNewsList(strings[0]);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return items;
    }

    @Override
    protected void onPostExecute(List<? extends BaseItem> list) {
        super.onPostExecute(list);
        baseListFragment.fillList(list);
//        pd.dismiss();
    }
}
