package com.vkuzub.newspn.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.base.BaseItemAdapter;
import com.vkuzub.newspn.model.BlogItem;

import java.util.List;

/**
 * Created by Vyacheslav on 19.07.2014.
 */
public class BlogItemAdapter extends BaseItemAdapter {


    public BlogItemAdapter(Context ctx, List<? extends BaseItem> items) {
        super(ctx, items);
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.listviewitem_blogs, viewGroup, false);
        }
        BlogItem item = (BlogItem) getItem(i);
        TextView tvBlogName = (TextView) view.findViewById(R.id.tvBlogName);
        TextView tvBlogSummary = (TextView) view.findViewById(R.id.tvBlogSummary);
        TextView tvBlogAuthor = (TextView) view.findViewById(R.id.tvBlogAuthor);
        TextView tvBlogTime = (TextView) view.findViewById(R.id.tvBlogTime);
//        ImageView ivBlogImg = (ImageView) view.findViewById(R.id.ivBlogImg);
        //заполнение
//        ivBlogImg.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_launcher));
        tvBlogName.setText(item.getName());
        tvBlogSummary.setText(item.getArticle_summary());
        tvBlogAuthor.setText(item.getAuthor());
        tvBlogTime.setText(item.getTime());
        return view;
    }
}