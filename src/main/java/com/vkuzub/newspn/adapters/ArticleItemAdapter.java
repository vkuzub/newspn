package com.vkuzub.newspn.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.base.BaseItemAdapter;
import com.vkuzub.newspn.model.ArticleItem;

import java.util.List;

/**
 * Created by Vyacheslav on 19.07.2014.
 */
public class ArticleItemAdapter extends BaseItemAdapter {


    public ArticleItemAdapter(Context ctx, List<? extends BaseItem> items) {
        super(ctx, items);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.listviewitem_articles, viewGroup, false);
        }
        ArticleItem item = (ArticleItem) getItem(i);
        TextView tvArticleName = (TextView) view.findViewById(R.id.tvArticleName);
        TextView tvArticleSummary = (TextView) view.findViewById(R.id.tvArticleSummary);
        TextView tvArticleTime = (TextView) view.findViewById(R.id.tvArticleTime);
//        ImageView ivArticleImg = (ImageView) view.findViewById(R.id.ivArticleImg);
        //заполнение
//        ivArticleImg.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_launcher));
        tvArticleName.setText(item.getName());
        tvArticleSummary.setText(item.getArticle_summary());
        tvArticleTime.setText(item.getTime());
        return view;
    }
}