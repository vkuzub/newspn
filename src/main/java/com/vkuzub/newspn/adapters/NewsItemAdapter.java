package com.vkuzub.newspn.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.base.BaseItemAdapter;
import com.vkuzub.newspn.model.NewsItem;

import java.util.List;

/**
 * Created by Vyacheslav on 19.07.2014.
 */
public class NewsItemAdapter extends BaseItemAdapter {

    public NewsItemAdapter(Context ctx, List<? extends BaseItem> items) {
        super(ctx, items);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.listviewitem_news, viewGroup, false);
        }
        NewsItem item = (NewsItem) getItem(i);
        TextView tvNewsItemTime = (TextView) view.findViewById(R.id.tvNewsItemTime);
        TextView tvNewsItem = (TextView) view.findViewById(R.id.tvNewsItem);
        //style
        tvNewsItem.setTextColor(Color.rgb(0, 0, 0));
        tvNewsItemTime.setTextColor(Color.rgb(0, 0, 0));
        if (item.getSeriousness().equals(NewsItem.NewsItemSeriousness.Bold)) {
            tvNewsItem.setTypeface(tvNewsItem.getTypeface(), Typeface.BOLD);
            tvNewsItemTime.setTypeface(tvNewsItemTime.getTypeface(), Typeface.BOLD);
        } else if (item.getSeriousness().equals(NewsItem.NewsItemSeriousness.RedBold)) {
            tvNewsItem.setTextColor(Color.rgb(129, 2, 2));
            tvNewsItemTime.setTextColor(Color.rgb(129, 2, 2));
            tvNewsItem.setTypeface(tvNewsItem.getTypeface(), Typeface.BOLD);
            tvNewsItemTime.setTypeface(tvNewsItemTime.getTypeface(), Typeface.BOLD);
        }
        tvNewsItemTime.setText(item.getTime());
        tvNewsItemTime.setBackgroundColor(247247247);
        tvNewsItem.setText(item.getName());

        return view;
    }
}