package com.vkuzub.newspn.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import com.vkuzub.newspn.main.ArticleActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Vyacheslav on 20.07.2014.
 */
public class AsyncImageDownloader extends AsyncTask<ImageView, Void, Bitmap> {

    private ArticleActivity activity;
    private ImageView imgdestination;
    private int width = 0;

    public AsyncImageDownloader(int width) {
        this.width = width;
    }

    public void link(ArticleActivity activity) {
        this.activity = activity;
    }

    public void unlink() {
        activity = null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imgdestination.setImageBitmap(bitmap);
    }

    protected Bitmap doInBackground(ImageView... imageViews) {
        imgdestination = imageViews[0];
        return getBitmapFromURL((String) imgdestination.getTag());
    }

    private Bitmap getBitmapFromURL(String url) {
        Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
            PNUtils.calculateThumbnailSize(bm, width);
        } catch (IOException e) {
            Log.e("MyLogs", "Error getting the image from server : " + e.getMessage());
        }
        return bm;
    }


}


