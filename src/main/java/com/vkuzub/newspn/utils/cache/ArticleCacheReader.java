package com.vkuzub.newspn.utils.cache;

import android.content.Context;
import android.util.Log;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.utils.PNUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vyacheslav on 30.07.2014.
 * @deprecated
 */
public class ArticleCacheReader implements Runnable {

    public static final String CACHE_KEY = "cache_articles";

    private Context context;
    private List<Article> articles;
    private List<Article> out;

    public ArticleCacheReader(Context context, List<Article> out) {
        this.context = context;
        this.out = out;
    }

    public List<Article> getArticles() {
        return articles;
    }

    @Override
    public void run() {
        read();
    }

    private void read() {
        Log.d("MyLogs", "Попытка чтения из кэша");
        try {
            //noinspection unchecked
            articles = (List<Article>) PNUtils.readObject(context, CACHE_KEY);
            Log.d("MyLogs", "Прочитано из кэша" + articles.size());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Collections.copy(articles, out);
    }


}
