package com.vkuzub.newspn.utils.cache;

import android.content.Context;
import android.util.Log;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.parser.JsoupHtmlParser;
import com.vkuzub.newspn.utils.PNUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vyacheslav on 30.07.2014.
 *
 * @deprecated
 */
public class ArticleCacheWriter implements Runnable {

    public static final String CACHE_KEY = "cache_articles";

    private Context context;
    private List<? extends BaseItem> listItems;


    public ArticleCacheWriter(Context context, List<? extends BaseItem> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public void run() {
        write();
    }


    private void write() {
        ArrayList<Article> articles = new ArrayList<Article>();
        Log.d("MyLogs", "Попытка записи кэша");
        for (BaseItem baseItem : listItems) {
            try {
                articles.add(JsoupHtmlParser.parseArticle(baseItem.getUrl()));
                PNUtils.writeObject(context, CACHE_KEY, articles);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("MyLogs", "Записано в кэш" + articles.size());
    }
}
