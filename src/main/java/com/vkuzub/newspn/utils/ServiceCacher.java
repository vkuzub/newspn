package com.vkuzub.newspn.utils;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.main.MainActivity;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.parser.JsoupHtmlParser;
import com.vkuzub.newspn.xml.XmlWorker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Сервис, парсит список статей и сохраняет их в xml
 */
public class ServiceCacher extends IntentService {

    private List<? extends BaseItem> itemsList;
    private List<Article> articles;

    public ServiceCacher() {
        super("serviceCacher");
    }

    private void run() {
        if (itemsList == null) {
            Log.d("MyLogs", getClass().getCanonicalName() + "listIsNull cache process stopped");
            stopSelf();
            return;
        }

        try {
            createArticlesList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        XmlWorker worker = new XmlWorker(getApplicationContext());


        for (Article article : articles) {
            try {
                worker.writeArticle(article);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("MyLogs", getClass().getCanonicalName() + " " + articles.size() + " articles cached");
        PNUtils.isCached = true;

        stopSelf();
    }

    private void createArticlesList() throws IOException {
        articles = new ArrayList<Article>();

        for (BaseItem baseItem : itemsList) {
            articles.add(JsoupHtmlParser.parseArticle(baseItem.getUrl()));
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        String msg = null;
        int items = 0;
        boolean canCheck = itemsList != null ? itemsList.size() > 0 : false;
        if (canCheck) {
            items = itemsList.size();
            msg = getResources().getString(R.string.articles_cached);
        } else {
            msg = getResources().getString(R.string.articles_not_cached);
        }

        showNotification(msg, items);
    }

    private void showNotification(String text, int items) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setSmallIcon(R.drawable.ic_launcher_small).setContentTitle(getString(R.string.app_name))
                .setContentText(text).setAutoCancel(true).setTicker(getString(R.string.app_name))
                .setDefaults(Notification.DEFAULT_ALL).setContentInfo(String.valueOf(items)).setWhen(System.currentTimeMillis());

        //вызов MainActivity при нажатии на notification
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPI = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPI);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
//        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notificationManager.notify(1337, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        itemsList = (List<? extends BaseItem>) intent.getSerializableExtra("listForCache");
        run();
    }

}
