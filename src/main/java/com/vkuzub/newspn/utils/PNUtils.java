package com.vkuzub.newspn.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.*;

/**
 * Created by Vyacheslav on 20.07.2014.
 */
public class PNUtils {

    public static final String PREFERENCES_NAME = "prefs";
    public static final String INTENT_URL_EXTRA = "article_url";
    public static final String INTENT_CACHE_ARTICLE = "article_cache";
    public static final String TAG_WORKER_FRAGMENT = "worker_fragment";
    public static final String BUNDLE_ARTICLE_TAG = "ser_article";
    public static final String INTENT_PARSER_KEY_EXTRA = "parser_key";
    public static final String BUNDLE_LIST_KEY = "bundle_list";
    public static MODE WORK_MODE = MODE.Offline;
    public static boolean isCached;

    /**
     * Перечисление описывает режимы работы приложения
     */
    public static enum MODE {
        Offline, Online, FirstStart;

        public boolean isOnline() {
            return this.equals(Online);
        }

        public boolean isFirstStart() {
            return this.equals(FirstStart);
        }

    }


    public static final String LOG_TAG = "NEWS_PN";


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static void calculateThumbnailSize(Bitmap img, int screenWidth) {
        float bwidth = img.getWidth();
        float bheight = img.getHeight();
        float imgCoef = bwidth / bheight;

//        Log.d("MyLogs", "imgBefore"+bwidth + " " + bheight);
        if (bwidth > screenWidth) {
            bwidth = screenWidth;
            bheight = (int) (bheight / imgCoef);
            img = Bitmap.createScaledBitmap(img, (int) bwidth, (int) bheight, false);
        }
//        Log.d("MyLogs", "imgAfter"+bwidth + " " + bheight);
    }

    @Deprecated
    public static void writeObject(Context context, String key, Object object) throws IOException {
        FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    @Deprecated
    public static Object readObject(Context context, String key) throws IOException,
            ClassNotFoundException {
        FileInputStream fis = context.openFileInput(key);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return ois.readObject();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getFilesDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            Log.d("MyLogs", "PNUtils deleting xml files" + children.length);
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success) {
                    return false;
                }
            }
        }
        return dir != null ? dir.delete() : false;
    }


}
