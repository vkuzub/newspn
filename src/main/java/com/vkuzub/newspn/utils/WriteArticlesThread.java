package com.vkuzub.newspn.utils;

import android.content.Context;
import android.util.Log;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.parser.JsoupHtmlParser;
import com.vkuzub.newspn.xml.XmlWorker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс-утилита, парсит список статей и сохраняет их в xml
 * @deprecated
 */
public class WriteArticlesThread implements Runnable {

    private List<? extends BaseItem> itemsList;
    private Context context;
    private List<Article> articles;

    public WriteArticlesThread(Context context, List<? extends BaseItem> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @Override
    public void run() {

        if (itemsList == null) {
            Log.d("MyLogs", getClass().getCanonicalName() + "listIsNull cache process stopped");
            return;
        }

        try {
            createArticlesList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        XmlWorker worker = new XmlWorker(context);


        for (Article article : articles) {
            try {
                worker.writeArticle(article);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("MyLogs", getClass().getCanonicalName() + " " + articles.size() + " articles cached");
        PNUtils.isCached = true;
    }

    private void createArticlesList() throws IOException {
        articles = new ArrayList<Article>();

        for (BaseItem baseItem : itemsList) {
            articles.add(JsoupHtmlParser.parseArticle(baseItem.getUrl()));
        }

    }




}
