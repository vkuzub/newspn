package com.vkuzub.newspn.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.fragments.lists.OnSiteListItemClickListener;
import com.vkuzub.newspn.fragments.workers.TaskCallbacks;
import com.vkuzub.newspn.fragments.workers.WorkerFragmentListItems;
import com.vkuzub.newspn.parser.AsyncListItemsParser;
import com.vkuzub.newspn.utils.PNUtils;
import com.vkuzub.newspn.utils.ServiceCacher;
import com.vkuzub.newspn.xml.XmlWorker;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Vyacheslav on 26.07.2014.
 */
public abstract class BaseListFragment extends ListFragment implements TaskCallbacks {

    protected ActionBarActivity activity;                                       //активити, требуется для операция в классах-наследниках

    protected OnSiteListItemClickListener onSiteListItemClickListener;          //слушатель нажатия
    private WorkerFragmentListItems workerFragment;                             //фрагмент рабочий - парсит список
    private XmlWorker xmlWorker;                                                //xml рабочий - парсит список с xml файла


    /**
     * Метод возвращает заполненный элеметами список из наследников
     *
     * @return - список элеметов
     */

    public abstract List<? extends BaseItem> getListItems();

    /**
     * Метод заполнения списка элементами
     *
     * @param items - элементы для заполнения
     */
    public abstract void fillList(List<? extends BaseItem> items);

    /**
     * Метод заполнения списка элементами из кэша
     *
     * @param items   - элементы для заполнения
     * @param context - контекст
     * @deprecated
     */
    public abstract void fillListCache(List<? extends BaseItem> items, Context context);

    /**
     * Метод производит инициализацию - запускает асинхронный парсер
     *
     * @deprecated
     */

    public void initContent() {
        AsyncListItemsParser parser = new AsyncListItemsParser(getParserKey());
        parser.link(this);
        parser.execute(createURL());
    }

    /**
     * Метод создает Url страницы, которую необходимо распарсить
     *
     * @return готовый url
     */

    protected abstract String createURL();

    /**
     * Метод возвращает Types - тип элементов
     *
     * @return - тип элементов
     */
    protected abstract BaseItem.TYPES getItemType();

    /**
     * Возвращает символ, необходимый для определения типа списка, парсеру
     *
     * @return символ для парсера
     */

    public abstract char getParserKey();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onSiteListItemClickListener = (OnSiteListItemClickListener) activity;
        this.activity = (ActionBarActivity) activity;
        xmlWorker = new XmlWorker(activity);
        Log.d("MyLogs", getClass().getCanonicalName() + " onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (savedInstanceState != null) {
            Log.d("MyLogs", getClass().getCanonicalName() + " filled from bundle");
            fillList((List<? extends BaseItem>) savedInstanceState.getSerializable(PNUtils.BUNDLE_LIST_KEY));
            return;
        }


        if (PNUtils.WORK_MODE.isOnline()) {
            FragmentManager fragmentManager = this.activity.getSupportFragmentManager();
            fragmentManager.findFragmentByTag(PNUtils.TAG_WORKER_FRAGMENT);

            if (workerFragment == null) {
                Bundle args = new Bundle();
                args.putChar(PNUtils.INTENT_PARSER_KEY_EXTRA, getParserKey());
                args.putString(PNUtils.INTENT_URL_EXTRA, createURL());
                workerFragment = new WorkerFragmentListItems(this);
                workerFragment.setArguments(args);
                fragmentManager.beginTransaction().add(workerFragment, PNUtils.TAG_WORKER_FRAGMENT).commit();
            }
        } else {
            List<? extends BaseItem> listItems = null;
            try {
                listItems = xmlWorker.readList(getItemType());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            if (listItems != null) {
                Log.d("MyLogs", getClass().getCanonicalName() + " fillFromCache " + listItems.size() + " elements");
                fillList(listItems);
            } else {
                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.offline_mode), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getListItems() != null) {
            Log.d("MyLogs", getClass().getCanonicalName() + " writed to bundle" + getListItems().size());
            outState.putSerializable(PNUtils.BUNDLE_LIST_KEY, (java.io.Serializable) getListItems());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getListView().setSelector(R.drawable.list_selector);
    }

    @Override
    public void onDetach() {
        if (getListItems() != null && PNUtils.WORK_MODE.isOnline()) {
            try {
                xmlWorker.writeList(getListItems(), getItemType());
                Log.d("MyLogs", getClass().getCanonicalName() + "writeCache " + (getListItems() == null ? 0 : getListItems().size()) + " elements");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onDetach();
    }

    /**
     * При нажатии на элемеент возвращается его url
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        onSiteListItemClickListener.onItemSelected(((BaseItem) l.getAdapter().getItem(position)).getUrl());

    }


    @Override
    public void onPreExecute() {

    }

    @Override
    public void onProgressUpdate(int percent) {

    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onPostExecute(Object object) {
        List<? extends BaseItem> list = null;
        if (object instanceof List) {
            //noinspection unchecked
            list = (List<? extends BaseItem>) object;
            fillList(list);
        }
        if (PNUtils.WORK_MODE.isOnline() && !PNUtils.isCached) {
            Log.d("MyLogs", getClass().getCanonicalName() + " Trying write " + list.size() + " articles  to xml...");
            Intent cacher = new Intent(activity.getApplicationContext(), ServiceCacher.class);
            cacher.putExtra("listForCache", (java.io.Serializable) list);
            activity.startService(cacher);
        }
    }


}
