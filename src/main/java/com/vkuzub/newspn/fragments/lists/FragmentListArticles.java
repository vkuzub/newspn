package com.vkuzub.newspn.fragments.lists;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.adapters.ArticleItemAdapter;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.base.BaseItemAdapter;
import com.vkuzub.newspn.base.BaseListFragment;

import java.util.List;

/**
 * Created by Vyacheslav on 23.07.2014.
 */

//TODO сделать переход на страницу
public final class FragmentListArticles extends BaseListFragment {

    protected final char parserKey = 'a';
    protected final String BASE_URL = "https://news.pn/ru/articles/";
    protected List<? extends BaseItem> listItems;
    private BaseItem.TYPES type = BaseItem.TYPES.Article;

    @Override
    public List<? extends BaseItem> getListItems() {
        return listItems;
    }

    @Override
    public void fillList(List<? extends BaseItem> items) {
        if (items == null) {
            Toast.makeText(super.activity, super.getString(R.string.error_when_download), Toast.LENGTH_LONG).show();
            return;
        }
        this.listItems = items;
        BaseItemAdapter adapter = new ArticleItemAdapter(super.activity, items);
        setListAdapter(adapter);
    }

    @Deprecated
    public void fillListCache(List<? extends BaseItem> items, Context context) {
        throw new UnsupportedOperationException("Operation is not support");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public char getParserKey() {
        return parserKey;
    }

    protected String createURL() {
        return BASE_URL;
    }

    @Override
    protected BaseItem.TYPES getItemType() {
        return type;
    }


}
