package com.vkuzub.newspn.fragments.lists;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.vkuzub.newspn.R;
import com.vkuzub.newspn.adapters.NewsItemAdapter;

import com.vkuzub.newspn.base.BaseItemAdapter;
import com.vkuzub.newspn.base.BaseListFragment;
import com.vkuzub.newspn.base.BaseItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Vyacheslav on 23.07.2014.
 */

public final class FragmentListNews extends BaseListFragment {

    protected char parserKey = 'n';
    protected final String BASE_URL = "https://news.pn/ru/archive/";
    protected List<? extends BaseItem> listItems;
    private BaseItem.TYPES type = BaseItem.TYPES.News;


    @Override
    public List<? extends BaseItem> getListItems() {
        return listItems;
    }

    @Override
    public void fillList(List<? extends BaseItem> items) {
        if (items == null) {
            Toast.makeText(super.activity, super.activity.getResources().getString(R.string.error_when_download), Toast.LENGTH_LONG).show();
            return;
        }
        this.listItems = items;
        BaseItemAdapter adapter = new NewsItemAdapter(super.activity.getApplicationContext(), items);
        setListAdapter(adapter);
    }

    @Deprecated
    public void fillListCache(List<? extends BaseItem> items, Context context) {
        throw new UnsupportedOperationException("Operation is not support");
    }


    @Override
    protected String createURL() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return BASE_URL + dateFormat.format(date);
    }

    @Override
    protected BaseItem.TYPES getItemType() {
        return type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public char getParserKey() {
        return parserKey;
    }

}
