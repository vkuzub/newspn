package com.vkuzub.newspn.fragments.workers;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.parser.JsoupHtmlParser;

import java.io.IOException;

/**
 * Created by Vyacheslav on 30.07.2014.
 */
public class WorkerFragmentArticle extends Fragment {

    private TaskCallbacks callbacks;
    private Task task;

    @Override
    public void onAttach(Activity activity) {
        callbacks = (TaskCallbacks) activity;
        Log.d("MyLogs", getClass().getCanonicalName() + " OnAttach");
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        String articleUrl = getArguments().getString("url");
        task = new Task();
        task.execute(articleUrl);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    private class Task extends AsyncTask<String, Void, Article> {

        @Override
        protected Article doInBackground(String... strings) {
            Article article = null;
            try {
                article = JsoupHtmlParser.parseArticle(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return article;
        }

        @Override
        protected void onPreExecute() {
            if (callbacks != null) {
                callbacks.onPreExecute();
            }
        }

        @Override
        protected void onPostExecute(Article article) {
            if (callbacks != null) {
                callbacks.onPostExecute(article);
            }
        }

        @Override
        protected void onProgressUpdate(Void... voids) {
            if (callbacks != null) {

            }
        }

        @Override
        protected void onCancelled() {
            if (callbacks != null) {
                callbacks.onCancelled();
            }
        }
    }
}
