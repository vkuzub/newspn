package com.vkuzub.newspn.xml;

import android.content.Context;
import android.util.Xml;
import com.vkuzub.newspn.base.BaseItem;
import com.vkuzub.newspn.model.Article;
import com.vkuzub.newspn.model.ArticleItem;
import com.vkuzub.newspn.model.BlogItem;
import com.vkuzub.newspn.model.NewsItem;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс-утилита, записывает и считывает данные из xml
 */
public class XmlWorker {

    private Context context;

    public XmlWorker(Context context) {
        this.context = context;
    }

    public void writeList(List<? extends BaseItem> list, BaseItem.TYPES type) throws IOException {
        String filename = type.toString() + "s.xml";
        FileOutputStream fos;

        fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fos, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        serializer.startTag(null, "items");


        for (BaseItem item : list) {


            serializer.startTag(null, "item");

            serializer.startTag(null, "name");
            serializer.text(item.getName());
            serializer.endTag(null, "name");

            serializer.startTag(null, "url");

//            Pattern pattern = Pattern.compile("[0-9]+");
//
//            Matcher matcher = pattern.matcher(item.getUrl());
//
//            while (matcher.find()) {
//                serializer.text(matcher.group());
//            }
            serializer.text(item.getUrl());

            serializer.endTag(null, "url");

            serializer.startTag(null, "time");
            serializer.text(item.getTime());
            serializer.endTag(null, "time");

            if (type.equals(BaseItem.TYPES.News)) {
                serializer.startTag(null, "seriousness");
                serializer.text(((NewsItem) item).getSeriousness().toString());
                serializer.endTag(null, "seriousness");
            } else if (type.equals(BaseItem.TYPES.Article)) {
                serializer.startTag(null, "article_summary");
                serializer.text(((ArticleItem) item).getArticle_summary());
                serializer.endTag(null, "article_summary");
            } else if (type.equals(BaseItem.TYPES.Blog)) {
                serializer.startTag(null, "article_summary");
                serializer.text(((BlogItem) item).getArticle_summary());
                serializer.endTag(null, "article_summary");
                serializer.startTag(null, "author");
                serializer.text(((BlogItem) item).getAuthor());
                serializer.endTag(null, "author");
            }

            serializer.endTag(null, "item");
        }

        serializer.endTag(null, "items");
        serializer.endDocument();

        serializer.flush();

        fos.close();


    }

    public void writeListItem(BaseItem item, BaseItem.TYPES type) {

    }

    public List<? extends BaseItem> readList(BaseItem.TYPES type) throws IOException, XmlPullParserException {
        List<? extends BaseItem> items = null;

        String name = null, url;

        XmlPullParserFactory factory;

        FileInputStream fis;
        try {
            fis = context.openFileInput(type + "s.xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        factory = XmlPullParserFactory.newInstance();

        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput(fis, null);

        switch (type) {
            case News:
                items = readNewsItem(xpp);
                break;
            case Article:
                items = readArticleItem(xpp);
                break;
            case Blog:
                items = readBlogItem(xpp);
                break;
        }
        return items;
    }


    private ArrayList<NewsItem> readNewsItem(XmlPullParser xpp) throws XmlPullParserException, IOException {
        ArrayList<NewsItem> list = new ArrayList<NewsItem>();
        NewsItem item = null;
        String name = null, url = null, time = null;
        NewsItem.NewsItemSeriousness seriousness = NewsItem.NewsItemSeriousness.Normal;
        String tagName = "";
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                tagName = xpp.getName();
            } else if (eventType == XmlPullParser.TEXT) {
                assert tagName != null;
                if (tagName.equals("name")) {
                    name = xpp.getText();
                } else if (tagName.equals("url")) {
                    url = xpp.getText();
                } else if (tagName.equals("time")) {
                    time = xpp.getText();
                } else if (tagName.equals("seriousness")) {
                    seriousness = NewsItem.NewsItemSeriousness.valueOfIgnoreCase(xpp.getText());
                    item = new NewsItem(name, time, url, seriousness);
                    list.add(item);
                }
                tagName = "";
            }

            eventType = xpp.next();
        }
        return list;
    }

    private ArrayList<BlogItem> readBlogItem(XmlPullParser xpp) throws XmlPullParserException, IOException {
        ArrayList<BlogItem> list = new ArrayList<BlogItem>();
        BlogItem item = null;
        String name = null, url = null, time = null, author = null, article_summary = null;
        String tagName = null;
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                tagName = xpp.getName();
            } else if (eventType == XmlPullParser.TEXT) {
                assert tagName != null;
                if (tagName.equals("name")) {
                    name = xpp.getText();
                } else if (tagName.equals("url")) {
                    url = xpp.getText();
                } else if (tagName.equals("time")) {
                    time = xpp.getText();
                } else if (tagName.equals("article_summary")) {
                    article_summary = xpp.getText();
                } else if (tagName.equals("author")) {
                    author = xpp.getText();
                    item = new BlogItem(name, time, url, null, article_summary, author);
                    list.add(item);
                }
                tagName = "";
            }

            eventType = xpp.next();
        }
        return list;
    }

    private ArrayList<ArticleItem> readArticleItem(XmlPullParser xpp) throws XmlPullParserException, IOException {
        ArrayList<ArticleItem> list = new ArrayList<ArticleItem>();
        ArticleItem item = null;
        String name = null, url = null, time = null, article_summary = null;
        String tagName = "";
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                tagName = xpp.getName();
            } else if (eventType == XmlPullParser.TEXT) {
                assert tagName != null;
                if (tagName.equals("name")) {
                    name = xpp.getText();
                } else if (tagName.equals("url")) {
                    url = xpp.getText();
                } else if (tagName.equals("time")) {
                    time = xpp.getText();
                } else if (tagName.equals("article_summary")) {
                    article_summary = xpp.getText();
                    item = new ArticleItem(name, time, url, null, article_summary);
                    list.add(item);
                }
                tagName = "";
            }

            eventType = xpp.next();
        }
        return list;
    }


    public void deleteList(BaseItem.TYPES type) {
        context.deleteFile(type.toString() + "s.xml");
    }


    public void writeArticle(Article article) throws IOException {
        String filename = article.getArticleUrl() + ".xml";

        List<String> cachedArticles = Arrays.asList(context.fileList());
        if (cachedArticles.contains(filename)) {
            return;
        }

        FileOutputStream fos;

        fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fos, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        serializer.startTag(null, "article");

        serializer.startTag(null, "name");
        serializer.text(article.getTitle());
        serializer.endTag(null, "name");

        serializer.startTag(null, "body");
        serializer.text(article.getBody());
        serializer.endTag(null, "body");


        serializer.endTag(null, "article");
        serializer.endDocument();

        serializer.flush();

        fos.close();


    }

    public Article readArticle(String fileName) throws XmlPullParserException, IOException {
        String body = null;
        String name = null;

        String fileNameDigits = null;

        Pattern pattern = Pattern.compile("[0-9]+");

        Matcher matcher = pattern.matcher(fileName);

        while (matcher.find()) {
            fileNameDigits = matcher.group();
        }

        FileInputStream fis;
        try {
            fis = context.openFileInput(fileNameDigits + ".xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        XmlPullParserFactory factory;
        factory = XmlPullParserFactory.newInstance();

        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput(fis, null);

        String tagName = "";
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                tagName = xpp.getName();
            } else if (eventType == XmlPullParser.TEXT) {
                if (tagName != null ? tagName.equals("body") : false) {
                    body = xpp.getText();
                } else if (tagName != null ? tagName.equals("name") : false) {
                    name = xpp.getText();
                }
                tagName = "";
            }
            eventType = xpp.next();
        }
        return new Article(name, body, null, null, fileName);
    }

    public void deleteArticle(String url) {
        context.deleteFile(url + ".xml");
    }

}
