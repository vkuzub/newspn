package com.vkuzub.newspn.model;

import com.vkuzub.newspn.base.BaseItem;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class BlogItem extends BaseItem {

    private String picUrl;                       //URL картинки
    private String article_summary;              //Описание к статье
    private String author;                       // Автор статьи


    public String getPicUrl() {
        return picUrl;
    }

    public String getArticle_summary() {
        return article_summary;
    }


    public String getAuthor() {
        return author;
    }

    public BlogItem(String name, String time, String url, String picUrl, String article_summary, String author) {
        super(name, time, url,TYPES.Blog);
        this.picUrl = picUrl;
        this.article_summary = article_summary;
        this.author = author;
    }

    @Override
    public String toString() {
        return "BlogItem{" +
                "name='" + super.getName() + '\'' +
                ", time='" + super.getTime() + '\'' +
                ", url='" + super.getUrl() + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", article_summary='" + article_summary + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}


