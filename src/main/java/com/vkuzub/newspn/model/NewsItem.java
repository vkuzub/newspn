package com.vkuzub.newspn.model;

import com.vkuzub.newspn.base.BaseItem;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class NewsItem extends BaseItem {

    /**
     * Перечисление с существующими важностями новости
     */

    public enum NewsItemSeriousness {
        Normal, Bold, RedBold;

        public static NewsItemSeriousness valueOfIgnoreCase(String value) {
            if (value.equalsIgnoreCase("normal")) {
                return Normal;
            } else if (value.equalsIgnoreCase("bold")) {
                return Bold;
            } else if (value.equalsIgnoreCase("redbold")) {
                return RedBold;
            }
            return Normal;
        }
    }

    private NewsItemSeriousness seriousness;     //Важность новости - обычная, важная (bold), очень важная (red bold)

    public static final String URL_BASE = "http://news.pn";


    public NewsItemSeriousness getSeriousness() {
        return seriousness;
    }


    public NewsItem(String name, String time, String url, NewsItemSeriousness seriousness) {
        super(name, time, url, TYPES.News);
        this.seriousness = seriousness;
    }

    @Override
    public String toString() {
        return "NewsItem{" +
                "name='" + super.getName() + '\'' +
                ", time='" + super.getTime() + '\'' +
                ", url='" + super.getUrl() + '\'' +
                ", seriousness=" + seriousness +
                '}';
    }


}


