package com.vkuzub.newspn.model;

import com.vkuzub.newspn.base.BaseItem;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class ArticleItem extends BaseItem {

    private String picUrl;                       //URL картинки
    private String article_summary;              //Описание к статье


    public String getPicUrl() {
        return picUrl;
    }

    public String getArticle_summary() {
        return article_summary;
    }


    public ArticleItem(String name, String time, String url, String picUrl, String article_summary) {
        super(name, time, url, TYPES.Article);
        this.picUrl = picUrl;
        this.article_summary = article_summary;
    }

    @Override
    public String toString() {
        return "ArticleItem{" +
                "name='" + super.getName() + '\'' +
                ", time='" + super.getTime() + '\'' +
                ", url='" + super.getUrl() + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", article_summary='" + article_summary + '\'' +
                '}';
    }
}


