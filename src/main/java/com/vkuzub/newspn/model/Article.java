package com.vkuzub.newspn.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class Article implements Serializable {

    private String title;           //заголовок
    private String body;             //тело статьи
    private List<String> mediaFiles; //изображения к статье
    private String headerPictureUrl; //главное изображение
    private String articleUrl;

    public Article(String title, String body, List<String> mediaFiles, String headerPictureUrl, String articleUrl) {
        this.title = title;
        this.body = body;
        this.mediaFiles = mediaFiles;
        this.headerPictureUrl = headerPictureUrl;
        this.articleUrl = articleUrl;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public String getHeaderPictureUrl() {
        return headerPictureUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public List<String> getMediaFiles() {
        return mediaFiles;
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + title + '\'' +
                ", headerPictureUrl='" + headerPictureUrl + '\'' +
                ", body='" + body + '\'' +
                ", mediaFiles=" + mediaFiles +
                '}';
    }
}
